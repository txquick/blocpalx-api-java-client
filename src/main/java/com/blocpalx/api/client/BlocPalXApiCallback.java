package com.blocpalx.api.client;

@FunctionalInterface
public interface BlocPalXApiCallback<T> {

    /**
     * Called whenever a response comes back from the remote API
     *
     * @param response the expected response object
     */
    void onResponse(T response);

    /**
     * Called whenever an error occurs.
     *
     * @param cause the cause of the failure
     */
    default void onFailure(Throwable cause) {
    }
}
