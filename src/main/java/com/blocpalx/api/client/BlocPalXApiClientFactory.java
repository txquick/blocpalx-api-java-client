package com.blocpalx.api.client;

import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.impl.BlocPalXApiAsyncRestClientImpl;
import com.blocpalx.api.client.impl.BlocPalXApiRestClientImpl;
import com.blocpalx.api.client.impl.BlocPalXApiWebSocketClientImpl;

import static com.blocpalx.api.client.impl.BlocPalXApiServiceGenerator.getSharedClient;

/**
 * A factory for creating BlocPalXApi client objects.
 */
public class BlocPalXApiClientFactory {

    /**
     * Environment
     */
    private BlocPalXEnvironment env;

    /**
     * API Key
     */
    private String apiKey;

    /**
     * Secret.
     */
    private String secret;

    /**
     * Instantiates a new api client factory.
     *
     * @param apiKey the API key
     * @param secret the Secret
     */
    private BlocPalXApiClientFactory(BlocPalXEnvironment env, String apiKey, String secret) {
        this.env = env;
        this.apiKey = apiKey;
        this.secret = secret;
    }

    /**
     * New instance.
     *
     * @param apiKey the API key
     * @param secret the Secret
     * @return the api client factory
     */
    public static BlocPalXApiClientFactory newInstance(BlocPalXEnvironment env, String apiKey, String secret) {
        return new BlocPalXApiClientFactory(env, apiKey, secret);
    }

    /**
     * New instance without authentication.
     *
     * @return the api client factory
     */
    public static BlocPalXApiClientFactory newInstance(BlocPalXEnvironment env) {
        return new BlocPalXApiClientFactory(env,null, null);
    }

    /**
     * Creates a new synchronous/blocking REST client.
     */
    public BlocPalXApiRestClient newRestClient() {
        return new BlocPalXApiRestClientImpl(env, apiKey, secret);
    }

    /**
     * Creates a new asynchronous/non-blocking REST client.
     */
    public BlocPalXApiAsyncRestClient newAsyncRestClient() {
        return new BlocPalXApiAsyncRestClientImpl(env, apiKey, secret);
    }

    /**
     * Creates a new web socket client used for handling data streams.
     */
    public BlocPalXApiWebSocketClient newWebSocketClient() {
        return new BlocPalXApiWebSocketClientImpl(env, getSharedClient());
    }

    /**
     * Returns the current environment
     */
    public BlocPalXEnvironment getEnvironment() {
        return env;
    }
}
