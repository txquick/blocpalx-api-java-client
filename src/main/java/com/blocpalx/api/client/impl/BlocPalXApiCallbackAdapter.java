package com.blocpalx.api.client.impl;

import com.blocpalx.api.client.BlocPalXApiCallback;
import com.blocpalx.api.client.BlocPalXApiError;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import com.blocpalx.api.client.exception.BlocPalXApiException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

import static com.blocpalx.api.client.impl.BlocPalXApiServiceGenerator.getBlocPalXApiError;

/**
 * An adapter/wrapper which transforms a Callback from Retrofit into a BlocPalXApiCallback which is exposed to the client.
 */
public class BlocPalXApiCallbackAdapter<T> implements Callback<T> {

    private final BlocPalXApiCallback<T> callback;

    public BlocPalXApiCallbackAdapter(BlocPalXApiCallback<T> callback) {
        this.callback = callback;
    }

    public void onResponse(Call<T> call, Response<T> response) {
        if (BlocPalXApiConstants.OUTPUT_SERVER_RESPONSES) {
            System.out.println("  received response: " + response);
        }
        if (response.isSuccessful()) {
            callback.onResponse(response.body());
        } else {
            if (response.code() == 504) {
                // HTTP 504 return code is used when the API successfully sent the message but not get a response within the timeout period.
                // It is important to NOT treat this as a failure; the execution status is UNKNOWN and could have been a success.
                return;
            }
            try {
                BlocPalXApiError apiError = getBlocPalXApiError(response);
                onFailure(call, new BlocPalXApiException(apiError));
            } catch (IOException e) {
                onFailure(call, new BlocPalXApiException(e));
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable throwable) {
        if (throwable instanceof BlocPalXApiException) {
            callback.onFailure(throwable);
        } else {
            callback.onFailure(new BlocPalXApiException(throwable));
        }
    }
}
