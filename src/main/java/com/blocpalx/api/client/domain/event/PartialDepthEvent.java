package com.blocpalx.api.client.domain.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import com.blocpalx.api.client.domain.market.OrderBookEntry;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Depth delta event for a symbol.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartialDepthEvent {

    @JsonProperty("lastUpdateId")
    private long lastUpdateId;

    /**
     * Bid depth delta.
     */
    @JsonProperty("bids")
    private List<OrderBookEntry> bids;

    /**
     * Ask depth delta.
     */
    @JsonProperty("asks")
    private List<OrderBookEntry> asks;


    @Override
    public String toString() {
        return new ToStringBuilder(this, BlocPalXApiConstants.TO_STRING_BUILDER_STYLE)
                .append("lastUpdateId", lastUpdateId)
                .append("bids", bids)
                .append("asks", asks)
                .toString();
    }
}
