package com.blocpalx.api.client.domain.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * A deposit address for a given asset.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DepositAddress {

    private String address;

    private String addressTag;

    private String asset;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressTag() {
        return addressTag;
    }

    public void setAddressTag(String addressTag) {
        this.addressTag = addressTag;
    }

    @JsonSetter("tag")
    public void setTag(String tag) {
        this.addressTag = tag;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    @JsonSetter("coin")
    public void setCoin(String coin) {
        this.asset = coin;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, BlocPalXApiConstants.TO_STRING_BUILDER_STYLE)
                .append("address", address)
                .append("addressTag", addressTag)
                .append("asset", asset)
                .toString();
    }
}