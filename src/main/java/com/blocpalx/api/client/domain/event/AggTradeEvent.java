package com.blocpalx.api.client.domain.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import com.blocpalx.api.client.domain.market.AggTrade;

/**
 * An aggregated trade event for a symbol.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AggTradeEvent extends AggTrade {

    @JsonProperty("e")
    private String eventType;

    @JsonProperty("E")
    private long eventTime;

    @JsonProperty("s")
    private String symbol;

    @Override
    public String toString() {
        return new ToStringBuilder(this, BlocPalXApiConstants.TO_STRING_BUILDER_STYLE)
                .append("eventType", eventType)
                .append("eventTime", eventTime)
                .append("symbol", symbol)
                .append("aggTrade", super.toString())
                .toString();
    }
}
