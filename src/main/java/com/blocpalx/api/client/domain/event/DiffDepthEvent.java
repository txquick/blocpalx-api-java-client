package com.blocpalx.api.client.domain.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.blocpalx.api.client.constants.BlocPalXApiConstants;
import com.blocpalx.api.client.domain.market.OrderBookEntry;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Depth delta event for a symbol.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiffDepthEvent {

    @JsonProperty("e")
    private String eventType;

    @JsonProperty("E")
    private long eventTime;

    @JsonProperty("s")
    private String symbol;

    @JsonProperty("U")
    private long firstUpdateId;

    /**
     * updateId to sync up with updateid in /api/v1/depth
     */
    @JsonProperty("u")
    private long finalUpdateId;

    /**
     * Bid depth delta.
     */
    @JsonProperty("b")
    private List<OrderBookEntry> bids;

    /**
     * Ask depth delta.
     */
    @JsonProperty("a")
    private List<OrderBookEntry> asks;


    @Override
    public String toString() {
        return new ToStringBuilder(this, BlocPalXApiConstants.TO_STRING_BUILDER_STYLE)
                .append("eventType", eventType)
                .append("eventTime", eventTime)
                .append("symbol", symbol)
                .append("firstUpdateId", firstUpdateId)
                .append("finalUpdateId", finalUpdateId)
                .append("bids", bids)
                .append("asks", asks)
                .toString();
    }
}
