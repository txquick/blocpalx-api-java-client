package com.blocpalx.api.client.domain.general;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UserAsset {

    @JsonProperty("coin")
    private String coin;

    @JsonProperty("depositAllEnable")
    private boolean depositAllEnable;

    @JsonProperty("free")
    private String free;

    @JsonProperty("freeze")
    private String freeze;

    @JsonProperty("ipoable")
    private String ipoable;

    @JsonProperty("ipoing")
    private String ipoing;

    @JsonProperty("isLegalMoney")
    private boolean isLegalMoney;

    @JsonProperty("locked")
    private String locked;

    @JsonProperty("name")
    private String name;

    @JsonProperty("storage")
    private String storage;

    @JsonProperty("trading")
    private boolean trading;

    @JsonProperty("withdrawAllEnable")
    private boolean withdrawAllEnable;

    @JsonProperty("withdrawing")
    private String withdrawing;

    // @JsonProperty("networkList")
    // private List<AssetNetwork> networkList;

}
