package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.TimeInForce;
import com.blocpalx.api.client.domain.account.NewOrder;
import com.blocpalx.api.client.domain.account.NewOrderResponseType;
import com.blocpalx.api.client.domain.account.Order;
import com.blocpalx.api.client.domain.account.request.*;

import java.util.List;

/**
 * Examples on how to place orders, cancel them, and query account information.
 */
public class OrdersExample {

    public static void main(String[] args) {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(
                BlocPalXEnvironment.SANDBOX,
                ApiKeysForExamples.API_KEY,
                ApiKeysForExamples.API_SECRET);
        BlocPalXApiRestClient client = factory.newRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Getting list of open orders
        List<Order> openOrders = client.getOpenOrders(new OrderRequest("ETHBTC"));
        System.out.println(openOrders);

        // Getting list of all orders with a limit of 10
        List<Order> allOrders = client.getAllOrders(new AllOrdersRequest("ETHBTC").limit(10));
        System.out.println(allOrders);

        // Placing a test MARKET order
        client.newOrderTest(NewOrder.marketBuy("ETHBTC", "0.0123"));

        // Placing a real MARKET BUY order with limited spend allowance
        // client.newOrder(NewOrder.marketSell("ETHBTC", "0.00014", "").newOrderRespType(NewOrderResponseType.FULL));

        // Placing a test LIMIT order
        client.newOrderTest(NewOrder.limitBuy("ETHBTC", TimeInForce.GTC, "0.00001", "25000.11"));

        // Placing a real LIMIT order
        /*
        NewOrderResponse newOrderResponse = client.newOrder(NewOrder.limitBuy("XMRBTC", TimeInForce.GTC, "0.0001", "0.005").newOrderRespType(NewOrderResponseType.FULL));
        System.out.println(newOrderResponse);
        String clientOrderId = newOrderResponse.getClientOrderId();

        // Get status of the order
        Order order = client.getOrderStatus(new OrderStatusRequest("XMRBTC", clientOrderId));
        System.out.println(order);
        */

        // Canceling an order
        /*
        try {
            CancelOrderResponse cancelOrderResponse = client.cancelOrder(new CancelOrderRequest("XMRBTC", clientOrderId));
            System.out.println(cancelOrderResponse);
        } catch (BlocPalXApiException e) {
            System.out.println(e.getError().getMsg());
        }
        */
    }

}