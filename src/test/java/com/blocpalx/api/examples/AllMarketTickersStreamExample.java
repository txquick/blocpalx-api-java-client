package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiWebSocketClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;

/**
 * All market tickers channel examples.
 * <p>
 * It illustrates how to create a stream to obtain all market tickers.
 */
public class AllMarketTickersStreamExample {

    public static void main(String[] args) {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(BlocPalXEnvironment.SANDBOX);
        BlocPalXApiWebSocketClient client = factory.newWebSocketClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        client.onAllMarketTickersEvent(event -> {
            System.out.println(event);
        });
    }
}
