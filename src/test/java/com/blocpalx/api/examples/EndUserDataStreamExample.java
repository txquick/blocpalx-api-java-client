package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;

/**
 * User data stream endpoints examples.
 * <p>
 * It illustrates how to create a stream to obtain updates on a user account,
 * as well as update on trades/orders on a user account.
 */
public class EndUserDataStreamExample {

    public static void main(String[] args) {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(
                BlocPalXEnvironment.SANDBOX,
                ApiKeysForExamples.API_KEY,
                ApiKeysForExamples.API_SECRET);
        BlocPalXApiRestClient client = factory.newRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Use the listen key from when the connection was created in StartUserDataStreamExample
        String listenKey = ApiKeysForExamples.STREAM_LISTEN_KEY;

        client.closeUserDataStream(listenKey);
    }
}
