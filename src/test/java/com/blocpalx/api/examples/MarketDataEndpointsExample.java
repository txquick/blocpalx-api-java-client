package com.blocpalx.api.examples;

import com.blocpalx.api.client.BlocPalXApiClientFactory;
import com.blocpalx.api.client.BlocPalXApiRestClient;
import com.blocpalx.api.client.constants.BlocPalXEnvironment;
import com.blocpalx.api.client.domain.account.Order;
import com.blocpalx.api.client.domain.account.TradeHistoryItem;
import com.blocpalx.api.client.domain.market.*;
import com.blocpalx.api.client.domain.market.request.OrderBookDepthLimit;
import com.blocpalx.api.client.exception.BlocPalXApiException;

import java.util.List;

/**
 * Examples on how to get market data information such as the latest price of a symbol, etc.
 */
public class MarketDataEndpointsExample {

    public static void main(String[] args) {
        BlocPalXApiClientFactory factory = BlocPalXApiClientFactory.newInstance(BlocPalXEnvironment.SANDBOX);
        BlocPalXApiRestClient client = factory.newRestClient();

        System.out.println("Base URL is: " + client.getBaseUrl());

        // Getting depth of a symbol
        OrderBook orderBook = client.getOrderBook("XMRBTC", OrderBookDepthLimit.TEN);
        System.out.println(orderBook);
        // System.out.println(orderBook.getAsks());

        // Getting latest ticker statistics of a symbol
        TickerStatistics tickerStatistics = client.get24HrPriceStatistics("XMRBTC");
        System.out.println(tickerStatistics);

        // Getting latest ticker statistics of all symbols
        List<TickerStatistics> allTickers = client.getAll24HrPriceStatistics();
        System.out.println(allTickers);

        // Getting latest ticker statistics of a symbol
        TickerPrice tickerPrice = client.getPrice("XMRBTC");
        System.out.println(tickerPrice);

        // Getting all latest prices
        List<TickerPrice> allPrices = client.getAllPrices();
        System.out.println(allPrices);

        // Getting agg trades
        List<AggTrade> aggTrades = client.getAggTrades("XMRBTC");
        System.out.println(aggTrades);

        // Getting trades
        List<TradeHistoryItem> trades = client.getTrades("XMRBTC", 20);
        System.out.println(trades);

        // Weekly candlestick bars for a symbol
        List<Candlestick> candlesticks = client.getCandlestickBars("XMRBTC", CandlestickInterval.WEEKLY);
        System.out.println(candlesticks);

        // Getting all book tickers
        List<BookTicker> allBookTickers = client.getBookTickers();
        System.out.println(allBookTickers);

        // Getting book ticker for a symbol
        BookTicker bookTicker = client.getBookTicker("XMRBTC");
        System.out.println(bookTicker);

        // test exception handling
        try {
            client.getOrderBook("UNKNOWN", OrderBookDepthLimit.TEN);
        } catch (BlocPalXApiException e) {
            System.out.println(e.getError().getCode()); // -1121
            System.out.println(e.getError().getMsg());  // Invalid symbol
        }
    }
}
