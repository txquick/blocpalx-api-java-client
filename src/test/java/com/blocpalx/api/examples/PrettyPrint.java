package com.blocpalx.api.examples;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

@Data
public class PrettyPrint {

    public void printIt(Object object) {

        System.out.println("\n---- vvvv " + object.getClass().getName() + " vvvv ----");
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        System.out.println("---- ^^^^ " + object.getClass().getName() + " ^^^^ ----\n");
    }

}
